/*
 * The MIT License
 *
 * Copyright 2015 Marcus.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package nan.marcus.projects.main;

import java.util.LinkedList;
import java.util.List;
import nan.marcus.projects.readers.SequenceFileReader;
import org.biojava3.alignment.Alignments;
import org.biojava3.alignment.template.Profile;
import org.biojava3.core.sequence.DNASequence;
import org.biojava3.core.sequence.compound.NucleotideCompound;
import org.biojava3.core.util.ConcurrencyTools;

/**
 *
 * @author Marcus
 */
public class Entry {
    
    public static void main(String[] args) {
        
        SequenceFileReader reader = new SequenceFileReader();
        
        List<DNASequence> sequences = reader.read("data/sequences.txt");
        
        System.out.printf("orginal sequences: \n%s\n\n", sequences);
        
        /* Aligment ------------------------------------------------------------ */
        System.out.println("----- Multiple aligment: ");
        
        Profile<DNASequence, NucleotideCompound> profile = Alignments.getMultipleSequenceAlignment(sequences);
        
        System.out.printf("sequence matching: \n%s\n", profile);
        
        /* Profile ------------------------------------------------------------- */
        System.out.println("----- Profile: ");
        
        List<NucleotideCompound> compounds = new LinkedList<>();
        compounds.add(new DNASequence("C").getCompoundAt(1));
        compounds.add(new DNASequence("A").getCompoundAt(1));
        compounds.add(new DNASequence("T").getCompoundAt(1));
        compounds.add(new DNASequence("G").getCompoundAt(1));
        
        List<StringBuilder> printableResults = new LinkedList<>();
        StringBuilder C = new StringBuilder("C"); printableResults.add(C);
        StringBuilder A = new StringBuilder("A"); printableResults.add(A);
        StringBuilder T = new StringBuilder("T"); printableResults.add(T);
        StringBuilder G = new StringBuilder("G"); printableResults.add(G);
        
        for (int i = 1; i <= profile.getLength(); i++) {
            
            float[] profileWeights = profile.getCompoundWeightsAt(i, compounds);
            
            for (int j = 0; j < printableResults.size(); j++) {
                
                printableResults.get(j).append(" ").append(profileWeights[j]);
            }
        }
        
        for (StringBuilder printable : printableResults) {
            
            System.out.println(printable.toString());
        }
        
        System.out.println();
        
        /* Consensus ----------------------------------------------------------- */
        System.out.println("----- Consensus word: ");
        
        StringBuilder consensus = new StringBuilder();
        
        for (int i = 1; i <= profile.getLength(); i++) {
            
            float[] profileWeights = profile.getCompoundWeightsAt(i, compounds);
            
            MaxValue max = getMax(profileWeights);
            
            if (isAmbiguous(max, profileWeights)) {
                consensus.append("-");
            } else {
                consensus.append(max.getIndexAsNucleotideCompound());
            }
        }
        
        System.out.println(consensus.toString());
        
        /* not sure what this is, but there is noticeable thread lock at end without it */
        ConcurrencyTools.shutdown();
    }
    
    private static final float error = 0.0000000001f;
    
    public static Boolean saveEquality(float a, float b) {
        
        if (Math.abs(a - b) < error) {
            return true;
        }
        
        return false;
    }
    
    public static class MaxValue {
        
        private float max;
        private int index;
        
        public MaxValue(float max, int index) {
            
            this.max = max;
            this.index = index;
        }

        public float getMax() {
            return max;
        }

        public void setMax(float max) {
            this.max = max;
        }

        public int getIndex() {
            return index;
        }

        public void setIndex(int index) {
            this.index = index;
        }
        
        public String getIndexAsNucleotideCompound() {
            
            switch (index) {
                case 0: return "C";
                case 1: return "A";
                case 2: return "T";
                case 3: return "G";
                default: return "-";
            }
        }
    }
    
    public static MaxValue getMax(float[] vals) {
        
        float max = Float.MIN_VALUE;
        int maxIndex = -1;
        
        for (int i = 0; i < vals.length; i++) {
            
            if (max < vals[i]) {
                max = vals[i];
                maxIndex = i;
            }
        }
        
        return new MaxValue(max, maxIndex);
    }
    
    public static Boolean isAmbiguous(MaxValue max, float[] vals) {
        
        for (int i = 0; i < vals.length; i++) {
            
            if (i != max.getIndex() && saveEquality(max.getMax(), vals[i])) {
                return true;
            }
        }
        
        return false;
    }
}
