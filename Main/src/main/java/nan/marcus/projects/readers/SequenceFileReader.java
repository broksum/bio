package nan.marcus.projects.readers;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import org.biojava3.core.sequence.DNASequence;

public class SequenceFileReader {

    public List<DNASequence> read(String filePath) {

        try (FileReader freader = new FileReader(filePath);
            BufferedReader reader = new BufferedReader(freader)) {
            
            String sequence = reader.readLine();
            
            List<DNASequence> sequences = new LinkedList<>();
            
            while (sequence != null) {
                
                sequences.add(new DNASequence(sequence));
                
                sequence = reader.readLine();
            }
            
            reader.close();
            freader.close();
            
            return sequences;

        } catch (IOException ex) {
            
            System.err.println("Failed opening file: exception - " + ex.getMessage());
            System.exit(1);
            
            return null;
        }
    }
}
